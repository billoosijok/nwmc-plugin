<?php
/*
Plugin Name: NWMC Plugin
Description: Make Visual Composer do what you want. Can't beat 'em, hack 'em.
Version: 1.4.5
Author: Belal Sejouk, Andrew Erickson, Holly Stassens, Tannyr Ellingson
Author URI: https://northwestmediacollectiev.com
*/

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
    die( "Page not found" );
}

define('NWMC_PLUGIN_ROOT_FILE',  __FILE__ );
define('NWMC_PLUGIN_SLUG', plugin_basename( __FILE__ ));

define('NWMC_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
define('NWMC_PLUGIN_URL', plugin_dir_url( __FILE__ ));
define('NWMC_PLUGIN_ASSETS_URL', plugin_dir_url( __FILE__ ) . 'assets/');

define('NWMC_UICOMPONENTS_DIR', plugin_dir_path( __FILE__ ) . 'UIComponents/');

define('NWMC__BEAVER_BUILDER_MODULES_DIR', plugin_dir_path( __FILE__ ) . 'UIComponents/BeaverBuilderModules/');
define('NWMC__VISUAL_COMPOSER_MODULES_DIR', plugin_dir_path( __FILE__ ) . 'UIComponents/VisualComposerModules/');

// Including file that manages all template
require_once NWMC_PLUGIN_DIR . 'what-to-include.php';
require_once NWMC_PLUGIN_DIR . 'updater.php';