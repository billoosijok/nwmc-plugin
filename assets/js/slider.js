
function MagicSlider(options) {
    window.$ = window.$ || jQuery;

    this.sliderElements = $(options.selector);

    this.sliders = [];
    this.controls = {};

    this._autoRotateInterval = null;
    this._autoRotateIntervalFunction = null;
    this._clickTimeout = null;

    if (!this.sliderElements.length) {
        return;
    }

    this.init = () => {
        const that = this;

        this.sliderElements.each(function (i, el) {
            that.sliders.push(new _Slider( $(this) ));

            if (options.autoRotateEvery > 0) {
                $(el).hover(() => {
                    if (that._autoRotateInterval) {
                        clearInterval(that._autoRotateInterval);
                    }
            }, () => {
                    if ( that._autoRotateIntervalFunction ) {
                        that._autoRotateInterval = setInterval(that._autoRotateIntervalFunction, options.autoRotateEvery);
                    }
                });
            }

        });

        this._init_controls();
    };


    this._init_controls = () => {
        const that = this;

        $("[data-controls-for-slider]").each(function (index) {
            const selector = $(this).attr('data-controls-for-slider');

            for (let i = 0; i < that.sliders.length; i++) {
                if ( that.sliders[i].slider.filter(selector).length ) {
                    that.controls[selector] = that.controls[selector] || [];
                    that.controls[selector].push(that.sliders[i]);
                }
            }

            if (that.controls[selector]) {
                const prevButton = $(this).find('.prev');
                const nextButton = $(this).find('.next');

                prevButton.click(function (e) {
                    var theSelector = selector;

                    if (that._clickTimeout) return;

                    that._clickTimeout = setTimeout(() => {
                        that._clickTimeout = null;
                    }, options.transitionDuration);

                    that.controls[theSelector].forEach(slider => slider.prev());
                });

                nextButton.click(function () {
                    var theSelector = selector;

                    if (that._clickTimeout) return;

                    that._clickTimeout = setTimeout(() => {
                        that._clickTimeout = null;
                    }, options.transitionDuration);

                    console.log(that.controls, theSelector);
                    that.controls[theSelector].forEach( slider => slider.next() );
                });

                if (options.autoRotateEvery > 0) {
                    that._autoRotateIntervalFunction = () => {
                        nextButton.click();
                    };

                    that._autoRotateInterval = setInterval(that._autoRotateIntervalFunction, options.autoRotateEvery);

                    $(this).hover(() => {
                        if (that._autoRotateInterval) {
                            clearInterval(that._autoRotateInterval);
                        }
                    }, () => {
                        if ( that._autoRotateIntervalFunction ) {
                            that._autoRotateInterval = setInterval(that._autoRotateIntervalFunction, options.autoRotateEvery);
                        }
                    });
                }
            }

        });

    };

    this.init();
}

function _Slider(el, options) {
    this.slider =  el;

    this.images = null;

    this._upperIndexLimit = null;
    this._lowerIndexLimit = null;
    this._imagesMap = {};

    this.init = () => {
        this.images = this.slider.find(" .slide");
        this._lowerIndexLimit = (this.images.length % 2 === 0) ? (this.images.length / 2) - 1 : Math.floor(this.images.length / 2);
        this._lowerIndexLimit = this._lowerIndexLimit * -1;

        this._upperIndexLimit = Math.floor(this.images.length / 2);

        for (let i = 0; i < this.images.length; i++) {
            this._imagesMap[i + this._lowerIndexLimit] = this.images[i];
        }

        this._updateMap(0);
    };

    this.next = () => {
        this._updateMap(1);
    };

    this.prev = () => {
        this._updateMap(-1);
    };

    this._updateMap = (factor) => {
        const newMap = {};
        for (x in this._imagesMap) {
            if (this._imagesMap.hasOwnProperty(x)) {
                let newIndex = parseInt(x) + factor;

                if (newIndex > this._upperIndexLimit) {
                    const indexOverflow = newIndex - this._upperIndexLimit - 1;
                    newIndex = indexOverflow + this._lowerIndexLimit;

                } else if (newIndex < this._lowerIndexLimit) {
                    const indexOverflow = this._lowerIndexLimit - newIndex - 1;
                    newIndex = this._upperIndexLimit - indexOverflow;
                }
                newMap[newIndex] = this._imagesMap[x];

                this._imagesMap[x].setAttribute('data-index', newIndex)
            }
        }
        this._imagesMap = newMap;
    };


    this.init();
}