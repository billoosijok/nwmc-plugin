
jQuery(document).ready(function () {
    new Parallax({selector: '[data-parallax]'})
});

function Parallax(options) {
    var layers =  document.querySelectorAll(options.selector);
    var scrolling = false;

    if (layers.length) {
        init();
    }

    function init() {
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];
            layer.style.transform = "translateY(0px)";
            var transition = window.getComputedStyle(layer).getPropertyValue('transition');
            layer.style.transition = transition + ', transform 0.4s';
        }

        window.addEventListener('scroll', function (e) {
            scrolling = true;
        }.bind(this));

        setInterval(parallaxIt.bind(this), 250);
    }

    function parallaxIt () {
        if (scrolling) {
            scrolling = false;

            for (var i = 0; i < layers.length; i++) {
                var elemRect = layers[i].parentNode.getBoundingClientRect();
                layers[i].style.transform = "translateY("+ (elemRect.top/layers[i].getAttribute('data-parallax') + 10) +"px)";
            }
        }

    }
}

// jQuery(document).ready(function() {
//
//     var parallax = false,
//         layers = document.querySelectorAll("[data-parallax]");
//
//     for (i = 0, len = layers.length; i < len; i++) {
//         var transition = window.getComputedStyle(layers[i]).getPropertyValue('transition');
//         layers[i].style.transition = transition + ', transform 0.5s'
//     }
//     window.addEventListener('scroll', function(event) {
//         parallax = true
//     });
//     setInterval( function () {
//         if (parallax) {
//
//             var depth, i, layer, len, movement, topDistance, translate3d;
//             topDistance = window.pageYOffset;
//
//             for (i = 0, len = layers.length; i < len; i++) {
//                 layer = layers[i];
//                 depth = layer.getAttribute('data-parallax');
//                 movement = -(topDistance * depth);
//                 console.log(movement);
//                 translate3d = 'translate3d(0, ' + movement + 'px, 0)';
//                 layer.style['-webkit-transform'] = translate3d;
//                 layer.style['-moz-transform'] = translate3d;
//                 layer.style['-ms-transform'] = translate3d;
//                 layer.style['-o-transform'] = translate3d;
//                 layer.style.transform = translate3d;
//             }
//             parallax = false
//         }
//     }, 200 )
//
//
// });
