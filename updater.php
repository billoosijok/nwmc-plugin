<?php

require NWMC_PLUGIN_DIR . 'includes/plugin-update-checker-4.4/plugin-update-checker.php';

$updateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/billoosijok/nwmc-plugin/',
    NWMC_PLUGIN_ROOT_FILE,
    NWMC_PLUGIN_SLUG
);


//Optional: If you're using a private repository, specify the access token like this:
$updateChecker->setAuthentication('F-6_8s-Qs2zGiKd6G8oQ');

//Optional: Set the branch that contains the stable release.
$updateChecker->setBranch('master');
