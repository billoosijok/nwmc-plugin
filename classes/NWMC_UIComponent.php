<?php

class NWMC_UIComponent extends WP_Widget {

    // * REQUIRES TO BE DEFINED IN SUBCLASS
    public $slug;

    // Widget Properties
    public $name;
    public $category = "NWMC";
    public $description = 'A user interface component created by NWMC';
    public $icon = NWMC_PLUGIN_ASSETS_URL . '/troll.png';
    public $class = '';

    public function __construct( $support = ['widget', 'shortcode', 'vc', 'beaver'] ) {
        if ( array_search('widget', $support) !== false ) {
//            $this->_initWidget();
        }
    }

    public function styles ( $options ) { return; }

    public function scripts ( $options ) { return; }

    public function options_form ( $options = []) { return; }

    public function render ( $options ) { return; }

    public function init_styles ( $options = [] ) {

        if (!isset($GLOBALS['NWMC_UIComponent_styles_enqueued'])) {
            $GLOBALS['NWMC_UIComponent_styles_enqueued'] = [];
        }

        $files = $this->styles($options);
        $files = ( is_array($files) ) ? $files : [$files];

        $styles = '';

        foreach ($files as $file) {
            if (!isset($GLOBALS['NWMC_UIComponent_styles_enqueued'][$file])) {
                $GLOBALS['NWMC_UIComponent_styles_enqueued'][$file] = true;
                $styles .= file_get_contents($file);
            }
        }

        return "<style>$styles</style>";
    }

    public function init_scripts ( $options = [] ) {
        if (!isset($GLOBALS['NWMC_UIComponent_scripts_enqueued'])) {
            $GLOBALS['NWMC_UIComponent_scripts_enqueued'] = [];
        }

        $files = $this->scripts($options);
        $files = ( is_array($files) ) ? $files : [$files];

        $scripts = '';
        ?>

        <?php
            foreach ($files as $file) {
                if (!isset($GLOBALS['NWMC_UIComponent_scripts_enqueued'][$file])) {
                    $GLOBALS['NWMC_UIComponent_scripts_enqueued'][$file] = true;
                    $scripts .= file_get_contents($file);
                }
            }

        return "<script type='text/javascript'>$scripts</script>";
    }

    public function widget ( $args, $options ) {
        $widget_title = ( ! empty($options['title']) ) ? $options['title'] : null;

        if ($widget_title) {
            $widget_title = apply_filters('widget_title', $widget_title, $args, $this->id_base);
            $widget_title = $args['before_title'] . $widget_title . $args['after_title'];
        }

        echo $args['before_widget'];

        if ($widget_title) echo $widget_title;

        $this->_display_callback( $options );

        echo $args['after_widget'];

    }

    public function form ( $options ) {
        $options['_type'] = 'widget';
        $this->options_form($options);
    }

    public function update( $new_instance, $old_instance ) { return; }

    public function _vc_options () {
        $options = ['_type' => 'vc'];
        $options = $this->options_form( $options );

//        var_dump($options);

        if ($options) {
            // Title
            vc_map(
                array(
                    'name' => !empty($this->name) ? $this->name : $this->slug,
                    'base' => "NWMC_" . $this->slug,
                    'class' => 'NWMC_UIComponent ' . $this->class,
                    'icon' => $this->icon,
                    'category'      => $this->category,
                    'params' => $options
                ));
        }
    }

    public function _do_the_shortcode($options, $content = '') {
        return $this->_display_callback( $options, $content );
    }

    private function _display_callback($options = [], $content = '') {
        
            if ( !is_array($options) )
                $options = [];
                
            $output = '';

            $output .= $this->init_styles($options);

            if (method_exists($this, 'render_widget')) {
                $output .= $this->render_widget($options);

            } elseif (method_exists($this, 'render_shortcode')) {
                $output .= $this->render_shortcode($options);

            } else {
                if ($content)
                    $options['the_nested_content'] = $content;
                $output .= $this->render($options);
            }

            $output .= $this->init_scripts( $options );

            return $output;

    }

    private function _initWidget() {
        $name = (!empty($this->name)) ? $this->name : $this->slug;
        parent::__construct('NWMC_' . $this->slug, $name, [
            'description' => $this->description
        ]);
    }

    static function register( $support = ['widget', 'shortcode', 'vc', 'beaver'] ) {
        $instance = static::class;
        $instance = new $instance( $support );

        if ( empty($instance->slug) ) {
            if ( WP_DEBUG ) {
                throw new Error( static::class . ' requires $slug to be defined as a property' );
            }
            return;
        }

        if ( array_search('widget', $support) !== false ) {
            add_action( 'widgets_init', function() use ($instance) {
                register_widget( $instance );
            });
        }

        if ( array_search('vc', $support) !== false ) {
            add_action( 'vc_before_init', [$instance, '_vc_options'] );
        }

        if ( array_search('shortcode', $support) !== false || array_search('vc', $support) !== false) {
            add_shortcode( 'NWMC_' . $instance->slug, [$instance, '_do_the_shortcode'] );
        }

//        if ( array_search('beaver_builder', $support) !== false ) {
//            add_shortcode( 'NWMC_' . $instance->slug, [$instance, '_do_the_shortcode'] );
//        }

    }

}