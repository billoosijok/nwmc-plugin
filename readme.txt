=== NWMC Plugin ===
Tags: NWMC, Visual Composer, Beaver Builder
Requires at least: 4.6
Tested up to: 4.9.8
Stable tag: trunk
Requires PHP: 5.2.4
License: GNU General Public License v3.0
License URI: https://www.gnu.org/licenses/gpl-3.0.en.html

Make Visual Composer do what you want. Can't beat 'em, hack 'em.

== Changelog ==
Not much