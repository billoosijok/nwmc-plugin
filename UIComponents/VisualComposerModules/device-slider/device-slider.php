<?php

include_once NWMC_PLUGIN_DIR . 'classes/NWMC_UIComponent.php';

class NWMC_Device_Slider extends NWMC_UIComponent {

    public $slug = 'device-slider';
    public $name = "Device Slider";

    function styles( $options ) {
        return plugin_dir_path(__FILE__) . 'device-slider.css';
    }

    function scripts( $options ) {
        $deps = [
            NWMC_PLUGIN_DIR . 'assets/js/slider.js',
        ];

        if (isset($options['parallax_effect']) && $options['parallax_effect']) {
            array_push($deps, NWMC_PLUGIN_DIR . 'js/parallax.js');
        }

        return $deps;
    }

    function options_form ( $options = [] ) {
       return
           array(
               array(
                       'type' => 'textfield',
                       'value' => '',
                       'heading' => 'Title',
                       'param_name' => 'simple_textfield',
                   ),
                   // params group
                   array(
                       'type' => 'param_group',
                       'value' => '',
                       'param_name' => 'slides',
                       'heading' => 'Slides',
                       // Note params is mapped inside param-group:
                       'params' => array(
                           array(
                               'type' => 'attach_image',
                               'value' => '',
                               'heading' => 'Desktop Version',
                               'description' => 'Width: 510px, Height: 291px',
                               'param_name' => 'desktop_images',
                           ),
                           array(
                               'type' => 'attach_image',
                               'value' => '',
                               'heading' => 'iPad Version',
                               'description' => 'Width: 213px, Height: 160px',
                               'param_name' => 'ipad_images',
                           ),
                           array(
                               'type' => 'textfield',
                               'value' => '',
                               'heading' => 'Project Title',
                               'param_name' => 'titles',
                           ),
                           array(
                               'type' => 'textfield',
                               'value' => '',
                               'heading' => 'Project Link',
                               'param_name' => 'links',
                           )
                       )
                   ),
               // params group
               array(
                   'type' => 'dropdown',
                   'value' => array ( 'Right' => 'right', 'Left' => 'left' ),
                   'param_name' => 'align_bottom_wrapper',
                   'heading' => 'Project Name Alignment',
               ),

//               array(
//                   'type' => 'checkbox',
//                   'holder' => 'div',
//                   'class' => '',
//                   'heading' => __( 'Parallax Effect' ),
//                   'param_name' => 'parallax_effect',
//                   'description' => __( 'Enables a parallax effect on scroll' ),
//               ),
           );
    }

    function render ( $options ) {

        $options = shortcode_atts( array (
            'align_bottom_wrapper' => 'right',
            'slides' => []
        ), $options );

        $output = '';

        if (empty($options['slides'])) return;

        $slides = json_decode(urldecode($options['slides']));
        $slides = array_map(function ($slide) {
            $slide->desktop_images = wp_get_attachment_image( $slide->desktop_images, 'large' );
            $slide->ipad_images = wp_get_attachment_image( $slide->ipad_images, 'large' );

            return $slide;
        }, $slides);

        $kinda_id = rand(1000, 10000);
        $slider_classes = ['NWMC_device-slider', $kinda_id ];
        $slider_classes_srt = join(' ', $slider_classes);
        $slider_classes_selector = join('.', $slider_classes);
        $NWMC_PLUGIN_ASSETS_URL = NWMC_PLUGIN_ASSETS_URL;

        $this_dir_url = untrailingslashit(plugin_dir_url(__FILE__));

        $output = "<div class='NWMC_device-slider-wrapper'>";

        $output .= "
            <div class='primary-images $slider_classes_srt desktop' style='background-image: url($this_dir_url/assets/Desktop.png)'>
                <div class='slides-wrapper'>";
                    foreach ($slides as $slide) {
                        $output .= "<div class='slide'>{$slide->desktop_images}</div>";
                    }
        $output .= "</div></div>";

        $output .= "
            <div class='secondary-images $slider_classes_srt ipad' style='background-image: url($this_dir_url/assets/iPad.png)'>
                <div class='slides-wrapper'>";
                    foreach ($slides as $slide) {
                        $output .= "<div class='slide'>{$slide->ipad_images}</div>";
                    }
        $output .= "</div></div>";


        $output .= "
                <div class='bottom-wrapper ". $options['align_bottom_wrapper'] ."'>
                    <div class='slider-controls' data-controls-for-slider='.$slider_classes_selector'>
                        <span class='prev'> <img src='{$NWMC_PLUGIN_ASSETS_URL}Down_Arrow.svg'> </span>
                        <span class='next'> <img src='{$NWMC_PLUGIN_ASSETS_URL}Down_Arrow.svg'> </span>
                    </div>";
        $output .= "<div class='meta $slider_classes_srt'>";
                        foreach ($slides as $slide) {
                            $output .= "<div class='slide'><span class='project-title'>{$slide->titles}</span><span class='project-link'><a href='{$slide->links}'>view project</a></span></div>";
                        }
        $output .= "</div>
                </div>
            </div>";

        $output .= "<script>
                jQuery(document).ready(function () {
                        new MagicSlider({selector: '.$slider_classes_selector', transitionDuration: 500, autoRotateEvery: 3000});
                });
            </script>";

        return $output;

    }
}


NWMC_Device_Slider::register(['shortcode', 'vc']);