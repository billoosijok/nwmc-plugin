<?php
include_once NWMC_PLUGIN_DIR . 'classes/NWMC_UIComponent.php';

class NWMC_Confetti_Section extends NWMC_UIComponent {

    public $slug = 'confetti_section';
    public $name = "Confetti Section";

    // Some weird global variables needed to make the randomness of the shapes happen!
    static $global_counter = 0;
    static $should_chop;

    function styles( $options ) {
        return plugin_dir_path(__FILE__) . 'confetti-section.css';
    }

    function scripts( $options ) {
        $deps = [
//               NWMC_PLUGIN_ASSETS_URL . 'js/parallax.js',
        ];

//        if (isset($options['parallax_effect']) && $options['parallax_effect']) {
//            array_push($deps, NWMC_MODULES_DIR . 'js/parallax.js');
//        }

        return $deps;
    }

    function options_form ( $options = [] ) {
        vc_map( array(
            'name' => !empty($this->name) ? $this->name : $this->slug,
            'base' => "NWMC_" . $this->slug,
            'class' => 'NWMC_UIComponent ' . $this->class,
            'icon' => $this->icon,
            'category'      => $this->category,
            // "as_parent" => array('only' => 'single_img'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
            "content_element" => true,
            "show_settings_on_create" => true,
            "is_container" => true,
            "params" => array(
                // add params same as with any other content element
                array(
                    "type" => "textfield",
                    "heading" => __("Extra class name", "my-text-domain"),
                    "param_name" => "el_class",
                    "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "my-text-domain")
                ),
                array(
                    "type" => "dropdown",
                    "heading" => __("Opacity", "my-text-domain"),
                    "param_name" => "opacity",
                    "description" => __("Controls the opacity of the shapes", "my-text-domain"),
                    "value" => array ('0.5', '1.0')
                ),
                array(
                    'type' => 'css_editor',
                    'heading' => __( 'Css', 'my-text-domain' ),
                    'param_name' => 'css',
                    'group' => __( 'Design options', 'my-text-domain' ),
                ),
            ),
            "js_view" => 'VcColumnView'
        ) );

    }


    function render ( $options ) {
        extract(shortcode_atts( [
            'css' => '',
            'opacity' => '0.5',
            'the_nested_content' => '',
            'el_class' => ''
        ], $options ));

        $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->slug, $options );

        $output = '';
        $nested_content = $the_nested_content;

        $my_assets = untrailingslashit(plugin_dir_url(__FILE__)) . '/assets';

        if ($css) {
            $output = "<style>$css</style>";
        }

        $output .= '
        <div class="NWMC_confetti-out-wrapper ' . esc_attr( "$css_class {$el_class}") .'">        
            <div class="NWMC_confetti-parent" style="opacity: '. $opacity .'">
                <div class="NWMC_confetti-wrapper" data-parallax="18" style="top: 1%; right: 35%;"><img class="NWMC_confetti" src="' . $my_assets . '/form.svg" aria-hidden="true"></div>
                <div class="NWMC_confetti-wrapper" data-parallax="18" style="bottom: 1%; left: 83%;"><img class="NWMC_confetti" src="' . $my_assets . '/zigzag.svg" aria-hidden="true"></div>
                <div class="NWMC_confetti-wrapper" data-parallax="10" style="top: 1%; right: 64%;"><img class="NWMC_confetti" src="' . $my_assets . '/ellipse.svg" aria-hidden="true"></div>
                <div class="NWMC_confetti-wrapper" data-parallax="14" style="bottom: -1%; left: 38%;"><img class="NWMC_confetti" src="' . $my_assets . '/plus.svg" aria-hidden="true"></div>
                <div class="NWMC_confetti-wrapper" data-parallax="15" style="top: 7%; right: 86%;"><img class="NWMC_confetti" src="' . $my_assets . '/square.svg" aria-hidden="true"></div>
                <div class="NWMC_confetti-wrapper" data-parallax="11" style="bottom: -15%; left: -5%;"><img class="NWMC_confetti" src="' . $my_assets . '/circle.svg" aria-hidden="true"></div>
            </div>
            <div class="NWMC_confetti-content">' . do_shortcode( $nested_content ) . '</div>
        </div>';
        return $output;

    }
}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_NWMC_Confetti_Section extends WPBakeryShortCodesContainer {
    }
}

NWMC_Confetti_Section::register( [ 'vc' ] );

NWMC_Confetti_Section::$should_chop = rand(0, 1);
function get_a_random_positions($minY = -10, $maxY = 9) {

    $sidesX = ['right', 'left'];
    $sidesY = ['top', 'bottom'];

    if (NWMC_Confetti_Section::$should_chop) {
        $sidesX = array_reverse( $sidesX );
        $sidesY = array_reverse( $sidesY );
    }

    $randomSideX = NWMC_Confetti_Section::$global_counter % count($sidesX);
    $randomSideY = NWMC_Confetti_Section::$global_counter % count($sidesY);

    $randDistY = rand($minY, $maxY);
    $randDistX = rand(0, 90);

    NWMC_Confetti_Section::$global_counter++;

    return "$sidesY[$randomSideY]: $randDistY%; $sidesX[$randomSideX]: $randDistX%;";
}