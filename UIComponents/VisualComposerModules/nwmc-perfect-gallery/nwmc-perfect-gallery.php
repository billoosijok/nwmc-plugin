<?php
/*
 * File for creating Title Component
 */

$GLOBALS['awe_groupnumb'] = 300;

/**
 * Function for Adding Title Component on vc_init hook
 *
 * @param void
 *
 * @return void
 */
function nwmc_perfect_component() {
	// Title
	vc_map(
		array(
			'name' => __( 'Perfect Gallery' ),
			'base' => 'nwmc_perfect_gallery',
			'class' => '',
			'icon' => plugins_url( 'bynwmc' ) . '/icons/troll.png', // Pass url to your icon here
			'category'      => __( 'By NWMC' ),
			'params' => array(
				array(
					"type" => "attach_images",
					"class" => "",
					"heading" => __( "Select Images"),
					"param_name" => "perf_imgs",
					"value" => '',
					"description" => __( "Select the images you would like to add to the gallery.")
				),
				array(
					"type" => "checkbox",
					"class" => "",
					"heading" => __( "Show Navigation Dots?"),
					"param_name" => "visible_dots",
					"description" => __( "Would you like to display the navigation dots?" )
				)
			)
		)
	);
}
add_action( 'vc_before_init', 'nwmc_perfect_component' );

/**
 * Function for displaying Title functionality
 *
 * @param array $atts    - the attributes of shortcode
 * @param string $content - the content between the shortcodes tags
 *
 * @return string $html - the HTML content for this shortcode.
 */
function nwmc_perfect_gallery_fnct( $atts, $content ) {
	$atts = shortcode_atts(
		array(
			'perf_imgs' => '',
			'visible_dots' => false
		), $atts, 'nwmc_perfect_gallery'
	);
	$images = explode(',', $atts['perf_imgs']);

	$images = array_map(function ($imageID) {
		return wp_get_attachment_image( $imageID, 'large' );
	}, $images);
	
	$size = 'full';
	$counterImg = 0;
	$groupnumb = $GLOBALS['awe_groupnumb'];
	$total = count($images);
	$html = '
	<div class="used-image-gallery">
		<input class="lightbox-buttons-on" type="radio" name="' . $groupnumb . '-lightbox" id="lightbox-on' . $groupnumb . '">
		<label for="lightbox-on' . $groupnumb . '" class="lightbox-label on"><span class="lightbox-label-text"><i class="fa fa-expand"></i></span></label>
		<input class="lightbox-buttons-off" type="radio" name="' . $groupnumb . '-lightbox" id="lightbox-off' . $groupnumb . '">
		<label for="lightbox-off' . $groupnumb . '" class="lightbox-label off"><span class="lightbox-label-text">X</span></label>
		<div class="lightbox-wrapper">';
		
		foreach( $images as $image ) {
		$html .= '<div class="single-used-image">';
				$html .= '
				<input class="top-nav-buttons" type="radio" name="' . $groupnumb . '-image" id="used-img-' . ($counterImg + 1) . '' . $groupnumb . '"' . ($counterImg === 0 ? "checked" : "") . '>
				<label for="used-img-' . ($counterImg + 1) . '' . $groupnumb . '" class="standard-label" style="' . ($atts['visible_dots'] ? "visibility: visible;" : "visibility: hidden;") . '"><span class="top-nav-label-text">image ' . ($counterImg + 1) . ' of ' . $total . '</span>
					<div class="check"></div>
				</label>';
			$html .= '
			<section class="used-img-' . ($counterImg + 1) . '">
				<div class="nav-prev">';
					if ($counterImg === 0) { 
						$html .= '<label for="used-img-' . $total . '' . $groupnumb . '" class="prev-label"><i class="fa fa-chevron-left"></i><span class="nav-label-text">Previous</span></label>';
					} else { 
						$html .= '<label for="used-img-' . ($counterImg) . '' . $groupnumb . '" class="prev-label"><i class="fa fa-chevron-left"></i><span class="nav-label-text">Previous</span></label>';
					}
				$html .= '
				</div>
				<div class="nav-next">';
					if ( $counterImg + 1 < $total) {
						$html .= '<label for="used-img-' . ($counterImg + 2) . '' . $groupnumb . '" class="next-label"><i class="fa fa-chevron-right"></i><span class="nav-label-text">Next</span></label>';
					} else if ($counterImg + 1 === $total) {
						$html .= '<label for="used-img-' . 1 . '' . $groupnumb . '" class="next-label"><i class="fa fa-chevron-right"></i><span class="nav-label-text">Next</span></label>';
					}
				$html .= '
				</div>' . $image . '
			</section>
		</div>';

			$counterImg ++;
		}

	$html .= '
		</div>
	</div>';
$GLOBALS['awe_groupnumb'] = $GLOBALS['awe_groupnumb'] + 100;
return $html;

}

add_shortcode( 'nwmc_perfect_gallery', 'nwmc_perfect_gallery_fnct' );
