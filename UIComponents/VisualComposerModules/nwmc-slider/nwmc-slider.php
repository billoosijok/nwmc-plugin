<?php

include_once NWMC_PLUGIN_DIR . 'classes/NWMC_UIComponent.php';

class NWMC_Double_Slider extends NWMC_UIComponent {

    public $slug = 'double-slider';
    public $name = "Double Slider";

    function styles( $options ) {
        return plugin_dir_path(__FILE__) . 'nwmc-slider.css';
    }

    function scripts( $options ) {
        $deps = [
            NWMC_PLUGIN_DIR . 'assets/js/slider.js',
        ];

        if (isset($options['parallax_effect']) && $options['parallax_effect']) {
            array_push($deps, NWMC_PLUGIN_DIR . 'js/parallax.js');
        }

        return $deps;
    }

    function options_form ( $options = [] ) {
       return
           array(
                array(
                    'type' => 'attach_images',
                    'holder' => 'div',
                    'class' => '',
                    'heading' => __( 'Image set' ),
                    'param_name' => 'images',
                    'description' => __( 'Images' ),
                ),
               array(
                   'type' => 'dropdown',
                   'holder' => 'div',
                   'class' => '',
                   'value'       => array(
                       'Default'   => 'default',
                       'Vertical'   => 'vertical',
                       '3D' => '3d'
                   ),
                   'heading' => __( 'Slider Type' ),
                   'param_name' => 'slider_type',
                   'description' => __( 'Select slider type' ),
               ),
               array(
                   'type' => 'dropdown',
                   'holder' => 'div',
                   'class' => '',
                   'value'       => array(
                       'Move' => 'move',
                       'Fade In/Out'   => 'fade',
                       'Fade In/Out & Move'   => 'fade move'
                   ),
                   'heading' => __( 'Slide Transition Type' ),
                   'param_name' => 'transition_type',
                   'description' => __( 'Select the animation type when transitioning between slides' ),
                   'group' => 'Animation Effects'
               ),
               array(
                   'type' => 'checkbox',
                   'holder' => 'div',
                   'class' => '',
                   'heading' => __( 'Display Controls' ),
                   'param_name' => 'display_controls',
                   'value' => 'true'
               ),
               array(
                   'type' => 'dropdown',
                   'value' => array ( 'Center' => 'center', 'Right' => 'right', 'Left' => 'left' ),
                   'param_name' => 'align_controls',
                   'heading' => 'Controls Alignment',
               ),
               array(
                   'type' => 'checkbox',
                   'holder' => 'div',
                   'class' => '',
                   'heading' => __( 'Parallax Effect' ),
                   'param_name' => 'parallax_effect',
                   'description' => __( 'Enables a parallax effect on scroll' ),
                   'group' => 'Animation Effects'
               ),
               array(
                   'type' => 'checkbox',
                   'holder' => 'div',
                   'class' => '',
                   'heading' => __( 'Enable Image Shadow' ),
                   'param_name' => 'image_shadow',
                   'description' => __( 'If checked, images will have box-shadow effect' ),
                   'group' => 'Style'
               ),
               array(
                   'type' => 'checkbox',
                   'holder' => 'div',
                   'class' => '',
                   'heading' => __( 'Enable Auto Rotate' ),
                   'param_name' => 'auto_rotate',
                   'description' => __( 'If checked, the slider will auto rotate every 3 seconds' ),
                   'value'  => 'true'
               ),
           );
    }

    function render ( $options ) {

        $output = '';

        $options = shortcode_atts( array(
            'images' => [],
            'slider_type' => 'regular',
            'parallax_effect' => false,
            'display_controls' => true,
            'transition_type' => 'move',
            'image_shadow'  => false,
            'align_controls' => 'center',
            'auto_rotate' => false
        ), $options );

        if (empty($options['images'])) return;

        $images = array_map(function ($imageID) {
            return wp_get_attachment_image( $imageID, 'large' );
        }, explode(',', $options['images']));

        $kinda_id = rand(1000, 10000);
        $slider_wrapper_classes = [( ( $options['parallax_effect'] ) ? 'parallax-layer' : '' ), ( ( $options['image_shadow'] ) ? 'image_shadow' : '' ) ];
        $slider_classes = ['double-slider', $kinda_id, $options['transition_type'] ];


        switch ( isset($options['slider_type']) ? $options['slider_type'] : null ) {
            case 'vertical':
                array_push($slider_classes, 'vertical');
                break;
            case '3d':
                array_push($slider_classes, 'three-d');
                break;

            default:
                array_push($slider_classes, 'regular');
        }

        $output .= '
        <div class="slider-wrapper ' . join(' ', $slider_wrapper_classes) . '">
            <div class="primary-images ' . join(' ', $slider_classes) . '">';

                foreach ($images as $image) {
                    $output .= "<div class='slide'>$image</div>";
                }
            $output .= "</div>";

            if ($options['display_controls']):

                $output .=   '<div class="slider-controls '. $options['align_controls'] .'" data-controls-for-slider=".' . str_replace(' ', '.', join('.', $slider_classes) ) . '">
                    <span class="prev"> <img src="' . NWMC_PLUGIN_ASSETS_URL . 'Down_Arrow.svg" alt=""> </span>
                    <span class="next"> <img src="' . NWMC_PLUGIN_ASSETS_URL . 'Down_Arrow.svg" alt=""> </span>
                </div>';
            endif;

        $output .= '</div>';

        $output .= '<script type="application/javascript">
            var selector_'.$kinda_id.' = ".' . $kinda_id . '.double-slider.three-d, .' . $kinda_id .'.double-slider.regular, .'.$kinda_id . '.vertical.double-slider";
            jQuery(document).ready(function () {
                new MagicSlider({selector: selector_'.$kinda_id.', transitionDuration: 500 ' .  (($options['auto_rotate']) ? ', autoRotateEvery: 3000' : '') . ' });
            });
        </script>';

        return $output;
    }
}


NWMC_Double_Slider::register(['shortcode', 'vc']);