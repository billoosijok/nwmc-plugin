<?php

// Add the components that you want to load
$COMPONENNTS_TO_LOAD = [
//    'BeaverBuilderModules' => [
//        'modules' => [
//            'example',
//            'basic-example'
//        ],
//        'fields' =>  [
//            ['id' => 'example-custom-field', 'scripts' => [NWMC__BEAVER_BUILDER_MODULES_DIR . 'fields/example-custom-field/my-custom-field.js'], 'styles' => ['']]
//        ]
//    ],

    'VisualComposerModules' => [
        'nwmc-slider',
        'device-slider',
        'confetti-section',
        'nwmc-perfect-gallery',
    ]
];

/**
 * A class that handles loading custom modules and custom
 * fields if the builder is installed and activated.
 */
class NWMC_UIComponents_Loader {

    /**
     * Initializes the class once all plugins have loaded.
     */
    static public function init() {
        add_action( 'plugins_loaded', __CLASS__ . '::setup_hooks' );
    }

    /**
     * Setup hooks if the builder is installed and activated.
     */
    static public function setup_hooks() {
        global $COMPONENNTS_TO_LOAD, $vc_manager;

        if ( isset($COMPONENNTS_TO_LOAD['BeaverBuilderModules'])
             && count($COMPONENNTS_TO_LOAD['BeaverBuilderModules']['modules']) > 0
             && class_exists( 'FLBuilder' ) ) {

            // Load Beaver Builder modules.
            add_action( 'init', __CLASS__ . '::load_beaver_builder_modules' );

            // Register custom Beaver Builder fields.
            add_filter( 'fl_builder_custom_fields', __CLASS__ . '::register_fields' );

            // Enqueue custom field assets.
            add_action( 'init', __CLASS__ . '::enqueue_field_assets' );

        } elseif ( isset($vc_manager) AND
            isset($COMPONENNTS_TO_LOAD['VisualComposerModules'])
            AND count($COMPONENNTS_TO_LOAD['VisualComposerModules']) > 0) {
            self::load_visual_composer_modules();
        }
    }

    /**
     * Loads our custom modules.
     */
    static public function load_beaver_builder_modules() {
        global $COMPONENNTS_TO_LOAD;

        if (!isset($COMPONENNTS_TO_LOAD['BeaverBuilderModules']) || count($COMPONENNTS_TO_LOAD['BeaverBuilderModules']['modules']) < 1 )
            return;

        foreach ($COMPONENNTS_TO_LOAD['BeaverBuilderModules']['modules'] as $module) {
            $file_path = NWMC__BEAVER_BUILDER_MODULES_DIR . 'modules/' . $module . '/' . $module . '.php';

            if (file_exists($file_path)) {
                require_once $file_path;
            } else {
                throw new Exception("'$module/$module.php' not found. Make sure file name matches the folder name");
            }
        }
    }

    /**
     * Registers our custom fields.
     */
    static public function register_fields( $fields ) {
        global $COMPONENNTS_TO_LOAD;

        if (!isset($COMPONENNTS_TO_LOAD['BeaverBuilderModules'])
            || !isset($COMPONENNTS_TO_LOAD['BeaverBuilderModules']['fields'])
            || count($COMPONENNTS_TO_LOAD['BeaverBuilderModules']['fields']) < 1) {
            return $fields;
        }

        foreach ($COMPONENNTS_TO_LOAD['BeaverBuilderModules']['fields'] as $field) {
            $fields[$field] = NWMC__BEAVER_BUILDER_MODULES_DIR . 'fields/'.$field.'.php';
        }
        return $fields;
    }

    /**
     * Enqueues our custom field assets only if the builder UI is active.
     */
    static public function enqueue_field_assets() {
        global $COMPONENNTS_TO_LOAD;

        if (!isset($COMPONENNTS_TO_LOAD['BeaverBuilderModules'])
            || !isset($COMPONENNTS_TO_LOAD['BeaverBuilderModules']['fields'])
            || count($COMPONENNTS_TO_LOAD['BeaverBuilderModules']['fields']) < 1) {
            return;
        }

        wp_enqueue_style( 'my-custom-fields', FL_MODULE_EXAMPLES_URL . 'assets/css/fields.css', array(), '' );
        wp_enqueue_script( 'my-custom-fields', FL_MODULE_EXAMPLES_URL . 'assets/js/fields.js', array(), time(), true );

        foreach ($COMPONENNTS_TO_LOAD['BeaverBuilderModules']['fields'] as $field) {

            if (!isset($field['id']) || empty($field['id']) ) return;

            if ( isset( $field['scripts'] ) ) {
                foreach ($field['scripts'] as $i => $script) {
                    wp_enqueue_script( $field['id'] . '_' . $i, $script, array(), time(), true );
                }
            }

            if ( isset( $field['styles'] ) ) {
                foreach ($field['styles'] as $i => $style) {
                    wp_enqueue_style( $field['id'] . '_' . $i, $style, array(), time(), true );
                }
            }

        }
    }

    static public function load_visual_composer_modules () {
        global $COMPONENNTS_TO_LOAD;

        if (!isset($COMPONENNTS_TO_LOAD['VisualComposerModules'])
            || count($COMPONENNTS_TO_LOAD['VisualComposerModules']) < 1) {
            return;
        }

        foreach ($COMPONENNTS_TO_LOAD['VisualComposerModules'] as $module) {
            include_once NWMC__VISUAL_COMPOSER_MODULES_DIR . $module . '/' . $module . '.php';
        }

    }
}

NWMC_UIComponents_Loader::init();